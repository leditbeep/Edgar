<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Home
Route::get('/home', 'HomeController@index');
Route::post('/home', 'HomeController@store');

// Room
Route::get('/home/{home}/room', 'RoomController@index');
Route::post('/home/{home}/room', 'RoomController@store');

// Heater
Route::get('/room/{room}/heater', 'HeaterController@index');
Route::get('/room/{room}/heater/status', 'HeaterController@status');
Route::post('/room/{room}/heater', 'HeaterController@store');

// Sensor
Route::get('/room/{room}/sensor', 'SensorController@index');
Route::post('/room/{room}/sensor', 'SensorController@store');

// Observations
Route::get('/room/{room}/observation', 'ObservationController@index');
Route::get('/room/{room}/observation/last', 'ObservationController@last');
Route::post('/sensor/{sensor}/observation', 'ObservationController@store');

// Target
Route::get('/room/{room}/target', 'TargetController@index');
Route::get('/room/{room}/target/applicable', 'TargetController@applicable');
Route::post('/room/{room}/target', 'TargetController@store');
