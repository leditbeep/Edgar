<?php

namespace Tests\Unit\Entities;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Carbon\Carbon;
use App\Entities\Target;

class TargetTest extends TestCase
{
    use RefreshDatabase;

    public function testScopeIsActiveReturnsNullEnd()
    {
        $target = factory(\App\Entities\Target::class)->create([
            'start' => Carbon::yesterday(),
            'end' => null,
        ]);

        $this->assertTrue(Target::active()->get()->contains($target));
    }

    public function testScopeIsActiveReturnsNullStart()
    {
        $target = factory(\App\Entities\Target::class)->create([
            'start' => null,
            'end' => Carbon::tomorrow(),
        ]);

        $this->assertTrue(Target::active()->get()->contains($target));
    }

    public function testScopeIsActiveDoNotReturnNotYetStarted()
    {
        $target = factory(\App\Entities\Target::class)->create([
            'start' => Carbon::tomorrow(),
            'end' => Carbon::tomorrow(),
        ]);

        $this->assertFalse(Target::active()->get()->contains($target));
    }

    public function testScopeIsActiveDoNotReturnAlreadyEnded()
    {
        $target = factory(\App\Entities\Target::class)->create([
            'start' => Carbon::yesterday(),
            'end' => Carbon::yesterday(),
        ]);

        $this->assertFalse(Target::active()->get()->contains($target));
    }

    public function testScopeOrdersByCreationDateDesc()
    {
        $target24 = factory(\App\Entities\Target::class)->create([
            'start' => Carbon::yesterday(),
            'end' => Carbon::tomorrow(),
            'temperature' => '24',
        ]);

        // So creations date will be different
        sleep(2);

        $target43 = factory(\App\Entities\Target::class)->create([
            'start' => Carbon::yesterday(),
            'end' => Carbon::tomorrow(),
            'temperature' => '43',
        ]);

        // So creations date will be different
        sleep(2);

        $target18 = factory(\App\Entities\Target::class)->create([
            'start' => Carbon::yesterday(),
            'end' => Carbon::tomorrow(),
            'temperature' => '18',
        ]);

        $this->assertEquals(Target::active()->first()->temperature, $target18->temperature);
    }
}
