<?php

namespace Tests\Unit\Services;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Exceptions\ObservationNotFoundException;
use App\Exceptions\TargetTemperatureNotFoundException;
use App\Exceptions\HeaterNotFoundException;
use Mockery;
use Carbon\Carbon;

class ThermostatServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * heater service mock
     *
     * @var App\Service\HeaterService
     */
    protected $heaterMock;

    /**
     * Room
     *
     * @var App\Entities\Room;
     */
    protected $room;

    /**
     * Set up
     */
    public function setUp()
    {
        parent::setUp();


        $this->room = factory(\App\Entities\Room::class)->create();

        factory(\App\Entities\Target::class)->create([
            'temperature' => '20',
            'room_id' => $this->room->id,
            'start' => Carbon::yesterday(),
            'end' => Carbon::tomorrow(),
        ]);

        factory(\App\Entities\Sensor::class)->create([
            'room_id' => $this->room->id,
        ]);

        factory(\App\Entities\Heater::class)->create([
            'room_id' => $this->room->id,
        ]);
    }

    /**
     * Test handle method: the low observation should turn on the heater
     *
     * @return void
     */
    public function testHandleLowObservation()
    {
        $this->heaterMock = Mockery::mock(\App\Services\HeaterService::class);
        $this->app->instance('App\Services\HeaterService', $this->heaterMock);
        $this->heaterMock->shouldReceive('turnOn')->once();
        $this->heaterMock->shouldNotReceive('turnOff');

        factory(\App\Entities\Observation::class)->create([
            'temperature' => '14',
            'sensor_id' => $this->room->sensor()->first()->id,
            'created_at' => Carbon::yesterday(),
        ]);

        $thermostatService = resolve(\App\Services\ThermostatService::class);
        $response = $thermostatService->handle($this->room);

        $this->assertNull($response);

        Mockery::close();
    }

    public function testHandleHighObservation()
    {
        $this->heaterMock = Mockery::mock(\App\Services\HeaterService::class);
        $this->app->instance('App\Services\HeaterService', $this->heaterMock);
        $this->heaterMock->shouldReceive('turnOff')->once();
        $this->heaterMock->shouldNotReceive('turnOn');

        factory(\App\Entities\Observation::class)->create([
            'temperature' => '34',
            'sensor_id' => $this->room->sensor()->first()->id,
            'created_at' => Carbon::yesterday(),
        ]);

        $thermostatService = resolve(\App\Services\ThermostatService::class);
        $response = $thermostatService->handle($this->room);

        $this->assertNull($response);

        Mockery::close();
    }

    public function testNoObservationThrowsException()
    {
        $this->room->observation()->delete();
        $this->expectException(ObservationNotFoundException::class);

        $thermostatService = resolve(\App\Services\ThermostatService::class);
        $thermostatService->handle($this->room);
    }

    public function testNoTargetThrowsException()
    {
        $this->room->target()->delete();
        $this->expectException(TargetTemperatureNotFoundException::class);

        factory(\App\Entities\Observation::class)->create([
            'temperature' => '14',
            'sensor_id' => $this->room->sensor()->first()->id,
            'created_at' => Carbon::yesterday(),
        ]);

        $thermostatService = resolve(\App\Services\ThermostatService::class);
        $thermostatService->handle($this->room);
    }

    public function testNoHeaterThrowsException()
    {
        $this->room->heater()->delete();
        $this->expectException(HeaterNotFoundException::class);

        factory(\App\Entities\Observation::class)->create([
            'temperature' => '14',
            'sensor_id' => $this->room->sensor()->first()->id,
            'created_at' => Carbon::yesterday(),
        ]);

        $thermostatService = resolve(\App\Services\ThermostatService::class);
        $thermostatService->handle($this->room);
    }
}
