<?php

namespace Tests\Unit\Services;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Entities\HeaterStatus;

class HeaterServiceTest extends TestCase
{
    use RefreshDatabase;

    protected $heater;

    protected $heaterService;

    public function setUp()
    {
        parent::setUp();

        $this->heater = factory(\App\Entities\Heater::class)->create();

        $this->heaterService = resolve(\App\Services\HeaterService::class);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTurnOnUpdateHeaterStatus()
    {
        $this->heaterService->turnOn($this->heater);

        $this->assertDatabaseHas('heater_status', [
            'heater_id' => $this->heater->id,
            'status' => true,
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTurnOffUpdateHeaterStatus()
    {
        $this->heaterService->turnOff($this->heater);

        $this->assertDatabaseHas('heater_status', [
            'heater_id' => $this->heater->id,
            'status' => false,
        ]);
    }
}
