<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HeaterTest extends TestCase
{
    use RefreshDatabase;

    protected $room;
    protected $heater;

    /**
     * Set up test
     */
    public function setUp()
    {
        parent::setUp();

        $this->room = factory(\App\Entities\Room::class)->create();

        $this->heater = factory(\App\Entities\Heater::class)->create([
            'room_id' => $this->room->id,
        ]);
    }

    /**
     * Test one can get all heaters for a room
     *
     * @return void
     */
    public function testGetHeater()
    {
        $response = $this->json('GET', '/api/room/'.$this->room->id.'/heater');

        $response->assertStatus(200);

        $response->assertJsonFragment($this->heater->toArray());
    }

    /**
     * Test Store heater
     *
     * @return void
     */
    public function testStoreHeater()
    {
        $response = $this->json('POST', '/api/room/'.$this->room->id.'/heater', [
            'serial' => '1234',
            'name' => 'test name',
            'description' => 'test description',
        ]);

        $this->assertDatabaseHas('heaters', [
            'serial' => '1234',
            'name' => 'test name',
            'description' => 'test description',
            'room_id' => $this->room->id,
        ]);

        $response->assertStatus(200);
    }
}
