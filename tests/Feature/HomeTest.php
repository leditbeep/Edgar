<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeTest extends TestCase
{
    use RefreshDatabase;

    protected $home;

    public function setUp()
    {
        parent::setUp();

        $this->home = factory(\App\Entities\Home::class)->create();
    }

    /**
     * Test a user can retrieve home
     *
     * @return void
     */
    public function testGetHomes()
    {
        $response = $this->json('GET', '/api/home');

        $response->assertStatus(200);

        $response->assertJsonFragment($this->home->toArray());
    }

    /**
     * Test a user can store home
     *
     * @return void
     */
    public function testStoreHome()
    {
        $response = $this->json('POST', '/api/home', [
            'name' => 'test name',
            'description' => 'test description',
        ]);

        $this->assertDatabaseHas('homes', [
            'name' => 'test name',
            'description' => 'test description',
        ]);

        $response->assertStatus(200);
    }
}
