<?php

namespace Tests\Feature;

use Artisan;
use Mockery;
use App\Entities\Room;
use App\Entities\Target;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TargetTest extends TestCase
{
    use RefreshDatabase;

    protected $room;

    public function setUp()
    {
        parent::setUp();

        Artisan::call('db:seed');

        $this->room = Room::first();
    }


    /**
     * Retrieve list of targets
     *
     * @return Json list of target
     */
    public function testIndexTarget()
    {
        $response = $this->json('GET', '/api/room/'.$this->room->id.'/target');

        $response->assertStatus(200);
    }

    /**
     * Retrieve applicable target
     *
     * @return Json applicable target as Json
     */
    public function testApplicableTarget()
    {
        $response = $this->json('GET', '/api/room/'.$this->room->id.'/target/applicable');

        $response->assertStatus(200);
    }

    /**
     * Test Creation of a target
     *
     * @return void
     */
    public function testCreateTarget()
    {
        $thermostatMock = Mockery::mock(\App\Services\ThermostatService::class);
        $this->app->instance('App\Services\ThermostatService', $thermostatMock);
        $thermostatMock->shouldReceive('handle')->once();

        $response = $this->json('POST', '/api/room/'.$this->room->id.'/target', [
            'temperature' => '3.1',
            'type' => Target::MANUAL,
        ]);

        $this->assertDatabaseHas('targets', [
            'temperature' => '3.1',
            'room_id' => $this->room->id,
            'type' => Target::MANUAL,
        ]);

        $response->assertStatus(200);

        Mockery::close();
    }

    /**
     * Temerature can be negative
     *
     * @return void
     */
    public function testCreateNegativeTarget()
    {
        $response = $this->json('POST', '/api/room/'.$this->room->id.'/target', [
            'temperature' => '-3.1',
            'type' => Target::MANUAL,
        ]);

        $this->assertDatabaseHas('targets', [
            'temperature' => '-3.1',
            'room_id' => $this->room->id,
            'type' => Target::MANUAL,
        ]);

        $response->assertStatus(200);
    }

    /**
     * Temperature is numeric
     *
     * @return void
     */
    public function testCannotCreateStringTemperatureTarget()
    {
        $response = $this->json('POST', '/api/room/'.$this->room->id.'/target', [
            'temperature' => 'absc',
            'type' => Target::MANUAL,
        ]);

        $response->assertStatus(422);
    }

    /**
     * Temperature can not be null
     *
     * @return void
     */
    public function testCannotCreateNullTemperatureTarget()
    {
        $response = $this->json('POST', '/api/room/'.$this->room->id.'/target', [
            'type' => Target::MANUAL,
        ]);

        $response->assertStatus(422);
    }

    /**
     * Type can not be null
     *
     * @return void
     */
    public function testCannotCreateNullTypeTarget()
    {
        $response = $this->json('POST', '/api/room/'.$this->room->id.'/target', [
            'temperature' => '39.3',
        ]);

        $response->assertStatus(422);
    }

    /**
     * Type must be integer
     *
     * @return void
     */
    public function testCannotCreateStringTypeTarget()
    {
        $response = $this->json('POST', '/api/room/'.$this->room->id.'/target', [
            'temperature' => '39.3',
            'type' => 'dflqkj',
        ]);

        $response->assertStatus(422);
    }
}
