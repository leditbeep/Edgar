<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoomTest extends TestCase
{
    use RefreshDatabase;

    protected $home;
    protected $room;

    /**
     * Set up test
     */
    public function setUp()
    {
        parent::setUp();
        
        $this->home = factory(\App\Entities\Home::class)->create();

        $this->room = factory(\App\Entities\Room::class)->create([
            'home_id' => $this->home->id,
        ]);
    }

    /**
     * Test on can get all rooms for a home
     *
     * @return void
     */
    public function testGetRoom()
    {
        $response = $this->json('GET', '/api/home/'.$this->home->id.'/room');

        $response->assertStatus(200);

        $response->assertJsonFragment($this->room->toArray());
    }

    /**
     * Test one can store a room
     *
     * @return void
     */
    public function testStoreRoom()
    {
        $response = $this->json('POST', '/api/home/'.$this->home->id.'/room', [
            'name' => 'test name',
            'description' => 'test description',
        ]);

        $this->assertDatabaseHas('rooms', [
            'name' => 'test name',
            'description' => 'test description',
            'home_id' => $this->home->id,
        ]);

        $response->assertStatus(200);
    }
}
