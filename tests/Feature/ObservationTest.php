<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;

class ObservationTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Testing the creation of an observation
     *
     * @return void
     */
    public function testCreateObservation()
    {
        $thermostatMock = Mockery::mock(\App\Services\ThermostatService::class);
        $this->app->instance('App\Services\ThermostatService', $thermostatMock);
        $thermostatMock->shouldReceive('handle')->once();

        $sensor = factory(\App\Entities\Sensor::class)->create();
        $heater = factory(\App\Entities\Heater::class)->create([
            'room_id' => $sensor->room->id,
        ]);

        $response = $this->json('POST', '/api/sensor/'.$sensor->serial.'/observation', [
            'temperature' => '3.1',
        ]);

        $this->assertDatabaseHas('observations', [
            'temperature' => '3.1',
            'sensor_id' => $sensor->id
        ]);

        $response->assertStatus(200);

        Mockery::close();
    }

    public function testCannotStoreNonNumericTemperature()
    {
        $sensor = factory(\App\Entities\Sensor::class)->create();
        $heater = factory(\App\Entities\Heater::class)->create([
            'room_id' => $sensor->room->id,
        ]);

        $response = $this->json('POST', '/api/sensor/'.$sensor->serial.'/observation', [
            'temperature' => 'abc',
            'sensor_id' => $sensor->id
        ]);

        $response->assertStatus(422);
    }

    public function testCannotStoreNullTemperature()
    {
        $sensor = factory(\App\Entities\Sensor::class)->create();
        $heater = factory(\App\Entities\Heater::class)->create([
            'room_id' => $sensor->room->id,
        ]);

        $response = $this->json('POST', '/api/sensor/'.$sensor->serial.'/observation', [
            'temperature' => null,
            'sensor_id' => $sensor->id
        ]);

        $response->assertStatus(422);
    }

    /**
     * Testing the listing of observations
     *
     * @return void
     */
    public function testListObservation()
    {
        $sensor = factory(\App\Entities\Sensor::class)->create();
        $heater = factory(\App\Entities\Heater::class)->create([
            'room_id' => $sensor->room->id,
        ]);
        $target = factory(\App\Entities\Target::class)->create([
            'room_id' => $sensor->room->id,
            'temperature' => '10',
        ]);
        $observation = factory(\App\Entities\Observation::class)->create([
            'sensor_id' => $sensor->id,
        ]);

        $response = $this->json('GET', '/api/room/'.$sensor->room->id.'/observation');

        $response->assertStatus(200);
    }

    /**
     * Testing the last observation
     *
     * @return void
     */
    public function testLastObservation()
    {
        $sensor = factory(\App\Entities\Sensor::class)->create();
        $heater = factory(\App\Entities\Heater::class)->create([
            'room_id' => $sensor->room->id,
        ]);
        $target = factory(\App\Entities\Target::class)->create([
            'room_id' => $sensor->room->id,
            'temperature' => '10',
        ]);
        $observation = factory(\App\Entities\Observation::class)->create([
            'sensor_id' => $sensor->id,
        ]);

        $response = $this->json('GET', '/api/room/'.$sensor->room->id.'/observation/last');

        $response->assertStatus(200);
    }
}
