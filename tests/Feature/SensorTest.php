<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SensorTest extends TestCase
{
    use RefreshDatabase;

    protected $room;
    protected $sensor;

    /**
     * Set up test
     */
    public function setUp()
    {
        parent::setUp();

        $this->room = factory(\App\Entities\Room::class)->create();

        $this->sensor = factory(\App\Entities\Sensor::class)->create([
            'room_id' => $this->room->id,
        ]);
    }

    /**
     * Test one can get all heaters for a room
     *
     * @return void
     */
    public function testGetSensor()
    {
        $response = $this->json('GET', '/api/room/'.$this->room->id.'/sensor');

        $response->assertStatus(200);

        $response->assertJsonFragment($this->sensor->toArray());
    }

    /**
     * Test Store heater
     *
     * @return void
     */
    public function testStoreSensor()
    {
        $response = $this->json('POST', '/api/room/'.$this->room->id.'/sensor', [
            'serial' => '1234',
            'name' => 'test name',
            'description' => 'test description',
        ]);

        $this->assertDatabaseHas('sensors', [
            'serial' => '1234',
            'name' => 'test name',
            'description' => 'test description',
            'room_id' => $this->room->id,
        ]);

        $response->assertStatus(200);
    }
}
