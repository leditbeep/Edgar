<div class="row">
    <div class="input-field col s4">
        <input placeholder="Ex: Living sensor" id="sensor_name" type="text" class="validate">
        <label for="sensor_name">Name</label>
    </div>
    <div class="input-field col s8">
        <input placeholder="Ex: The main sensor" id="sensor_description" type="text" class="validate">
        <label for="sensor_description">Description</label>
    </div>
    <div class="input-field col s12">
        <input placeholder="Ex: 6547895" id="sensor_serial" type="text" class="validate">
        <label for="sensor_serial">Serial Number</label>
    </div>
</div>
<a id="sensor_submit" class="waves-effect waves-light btn"><i class="material-icons left">save</i>Save Sensor</a>

<script type="text/javascript">
    $( document ).ready( function () {
        $( "#sensor_submit" ).on('click', function () {
            save_sensor();
        });
    });

    function save_sensor() {
        var name=$("#sensor_name").val();
        var description=$("#sensor_description").val();
        var serial=$("#sensor_serial").val();
        $.post("/api/room/{{ Request::segment(2) }}/sensor/", { name: name, description: description, serial: serial }).done(function (data) {
            location.reload(true);
        });
    }
</script>
