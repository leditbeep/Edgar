@extends('page')

@section('content')
    <h1>Choose your home</h1>



    <div class="row card-panel">
        @foreach($homes as $home)
            <div class="col s4">
                <a href="/home/{{ $home->id }}/room">
                    <h3>{{ $home->name }}</h3>
                </a>
                <p>{{ $home->description }}</p>

                <a class="btn btn-floating"><i class="material-icons">edit</i></a>
                <a class="btn btn-floating"><i class="material-icons">delete</i></a>
            </div>
        @endforeach
    </div>

    <div class="row">
        <h3>Create new home</h3>

        @include('home.form')
    </div>
@endsection
