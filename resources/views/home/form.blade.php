<div class="row">
    <div class="input-field col s4">
        <input placeholder="ex: Villa Medicis" id="home_name" type="text" class="validate">
        <label for="home_name">Name</label>
    </div>
    <div class="input-field col s8">
        <input placeholder="ex: the adress of the house" id="home_description" type="text" class="validate">
        <label for="home_description">Description</label>
    </div>
</div>
<a id="home_submit" class="waves-effect waves-light btn"><i class="material-icons left">save</i>Save</a>

<script type="text/javascript">
    $( document ).ready( function () {
        $( "#home_submit" ).on('click', function () {
            save_home();
        });
    });

    function save_home() {
        var name=$("#home_name").val();
        var description=$("#home_description").val();
        $.post("/api/home/", { name: name, description: description }).done(function (data) {
            location.reload(true);
        });
    }
</script>
