<div class="row">
    <div class="input-field col s4">
        <input placeholder="Ex: Living heater" id="heater_name" type="text" class="validate">
        <label for="heater_name">Name</label>
    </div>
    <div class="input-field col s8">
        <input placeholder="Ex: The main heater" id="heater_description" type="text" class="validate">
        <label for="heater_description">Description</label>
    </div>
    <div class="input-field col s12">
        <input placeholder="Ex: 12-5698-695-dfv" id="heater_serial" type="text" class="validate">
        <label for="heater_serial">Serial Number</label>
    </div>
</div>
<a id="heater_submit" class="waves-effect waves-light btn"><i class="material-icons left">save</i>Save Heater</a>

<script type="text/javascript">
    $( document ).ready( function () {
        $( "#heater_submit" ).on('click', function () {
            save_heater();
        });
    });

    function save_heater() {
        var name=$("#heater_name").val();
        var description=$("#heater_description").val();
        var serial=$("#heater_serial").val();
        $.post("/api/room/{{ Request::segment(2) }}/heater/", { name: name, description: description, serial: serial }).done(function (data) {
            location.reload(true);
        });
    }
</script>
