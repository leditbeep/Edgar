<div class="row">

    <div class="input-field col s4">
        <input placeholder="Ex: Living" id="room_name" type="text" class="validate">
        <label for="room_name">Name</label>
    </div>
    <div class="input-field col s8">
        <input placeholder="Ex: The main room" id="room_description" type="text" class="validate">
        <label for="room_description">Description</label>
    </div>
</div>
<a id="room_submit" class="waves-effect waves-light btn"><i class="material-icons left">save</i>Save</a>

<script type="text/javascript">
    $( document ).ready( function () {
        $( "#room_submit" ).on('click', function () {
            save_room();
        });
    });

    function save_room() {
        var name=$("#room_name").val();
        var description=$("#room_description").val();
        $.post("/api/home/{{ Request::segment(2) }}/room/", { name: name, description: description }).done(function (data) {
            location.reload(true);
        });
    }
</script>
