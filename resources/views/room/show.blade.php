@extends('page')

@section('content')

    <div class="row">
        <div class="col s12 m6">
            <div class="card blue-grey darken-1">
                <div class="card-content white-text center-align">
                    <h3><span id="targetTemperature"></span>°C</h3>

                    <a class="btn-large btn-floating left" id="increase"><i class="material-icons">add</i></a>
                    <a class="btn-large btn-floating right" id="decrease"><i class="material-icons">remove</i></a>
                </div>
                <div class="card-action white-text center-align">
                    <h5>{{ $room->name }}</h5   >
                    <p>Actual temperature: <span id="observedTemperature"></span>°C</p>
                    <i id="heaterStatus" class="material-icons center">whatshot</i>
                </div>
            </div>
        </div>
        <div class="col s12 m6 center-align">
            <h3>Actions</h3>
            <a class="btn"><i class="material-icons left">timeline</i>History</a>
            <a class="btn"><i class="material-icons left">date_range</i>Schedule</a>
        </div>
    </div>

    <div class="row">
        <div class="col s12 m6">
            <h3>Add a heater</h3>
            @include('heater.form')
        </div>
        <div class="col s12 m6">
            <h3>Add a sensor</h3>
            @include('sensor.form')
        </div>
    </div>

    <script type="text/javascript">
        var targetTemperature=1;
        var observedTemperature=1;
        var heaterStatus=false;
        var debouncedFunction = _.debounce(postTarget, 500);

        $( document ).ready(function () {
            refresh();
            setUp();
            $( "#increase" ).on('click', increaseTarget);
            $( "#decrease" ).on('click', decreaseTarget);
        });

        function setUp() {
            getTarget();
            getObservation();
            getHeaterStatus();
        }

        function increaseTarget() {
            targetTemperature=targetTemperature+0.5;
            setTarget(targetTemperature);
            debouncedFunction();
        }

        function decreaseTarget() {
            targetTemperature=targetTemperature-0.5;
            setTarget(targetTemperature);
            debouncedFunction();
        }

        function getTarget() {
            $.get("/api/room/"+{{ $room->id }}+"/target/applicable", function (response) {
                targetTemperature=jQuery.parseJSON(response).temperature;
                setTarget(targetTemperature);
            });
        }

        function postTarget() {
            $.post("/api/room/"+{{ $room->id }}+"/target", { temperature: readTarget(), type: '1' }).done(function (data) {
                getObservation();
            });
        }

        function setTarget(target) {
            $( "#targetTemperature" ).html(target);
        }

        function readTarget() {
            return $( "#targetTemperature" ).html();
        }

        function getObservation() {
            $.get("/api/room/"+{{ $room->id }}+"/observation/last", function (response) {
                observedTemperature=jQuery.parseJSON(response).temperature;
                setObservation(observedTemperature);
            });
        }

        function setObservation(observation) {
            $( "#observedTemperature" ).html(observation);
        }

        function getHeaterStatus() {
            $.get("/api/room/"+{{ $room->id }}+"/heater/status", function (response) {
                heaterStatus=jQuery.parseJSON(response).status;
                setHeaterStatus(heaterStatus);
            });
        }

        function setHeaterStatus(status) {
            if(status == true) {
                $( "#heaterStatus" ).addClass('orange-text');
            } else {
                $( "#heaterStatus" ).removeClass('orange-text');
            }
        }

        function refresh() {
            setInterval(function () {
                setUp();
                console.log('refresh data: ' + new Date());
            },20000);
        }
    </script>
@endsection
