@extends('page')

@section('content')
    <h1>Choose your room</h1>

    <div class="row card-panel">
        @foreach($rooms as $room)
            <div class="col s4">
                <a href="/room/{{ $room->id }}">
                    <h2>{{ $room->name }}</h2>
                </a>
                <p>{{ $room->description }}</p>

                <a class="btn btn-floating"><i class="material-icons">edit</i></a>
                <a class="btn btn-floating"><i class="material-icons">delete</i></a>
            </div>
        @endforeach
    </div>

    <div class="row">
        <h3>Create new room</h3>

        @include('room.form')
    </div>
@endsection
