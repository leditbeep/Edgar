<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class HeaterStatus extends Model
{
    /**
     * Arguments that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'status',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'heater_status';

    /**
     * Get the heater concerned by this status
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function heater()
    {
        return $this->belongsTo(\App\Entities\Heater::class);
    }
}
