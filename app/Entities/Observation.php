<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Observation extends Model
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'temperature'
    ];

    /**
     * Get the sensor from which the observation has been taken
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sensor()
    {
        return $this->belongsTo(\App\Entities\Sensor::class);
    }
}
