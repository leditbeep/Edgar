<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use \Illuminate\Database\Eloquent\Builder;

class Target extends Model
{
    const MANUAL = 1;
    const AUTOMATIC = 2;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'temperature',
        'type',
    ];

    /**
     * Get the Room of the target
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo(\App\Entities\Room::class);
    }

    /**
     * Return the presently valid target
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive(Builder $query)
    {
        return $query
            ->where(function (Builder $query) {
                return $query->whereNull('start')
                            ->orWhere('start', '<=', Carbon::now());
            })
            ->where(function (Builder $query) {
                return $query->whereNull('end')
                            ->orWhere('end', '>', Carbon::now());
            })
            ->orderBy('created_at', 'desc');
    }
}
