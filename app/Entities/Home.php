<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    /**
     * Fields that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];

    /**
     * Get the rooms of the Home
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function room()
    {
        return $this->hasMany(\App\Entities\Room::class);
    }
}
