<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Sensor extends Model
{
    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'serial',
        'name',
        'description',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'serial';
    }

    /**
     * Get the Room where the sensor is placed
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo Room
     */
    public function room()
    {
        return $this->belongsTo(\App\Entities\Room::class);
    }

    /**
     * Get the sensor observations
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany Observation
     */
    public function observation()
    {
        return $this->hasMany(\App\Entities\Observation::class);
    }
}
