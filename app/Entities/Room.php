<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    /**
     * Fields that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];

    /**
     * Get the home in which the sensor is placed
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function home()
    {
        return $this->belongsTo(\App\Entities\Home::class);
    }

    /**
     * Get the sensors placed in the Room
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sensor()
    {
        return $this->hasMany(\App\Entities\Sensor::class);
    }

    /**
     * Get the heaters relatives to the Room
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function heater()
    {
        return $this->hasMany(\App\Entities\Heater::class);
    }

    /**
     * Get the target conditions for the room
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function target()
    {
        return $this->hasMany(\App\Entities\Target::class);
    }

    /**
     * Get the observations for a room
     *
     * @return Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function observation()
    {
        return $this->hasManyThrough('App\Entities\Observation', 'App\Entities\Sensor');
    }
}
