<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Entities\Observation;

class Heater extends Model
{
    protected $fillable = [
        'serial',
        'name',
        'description',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'serial';
    }

    /**
     * Return the home where the heater is placed
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo(\App\Entities\Room::class);
    }

    /**
     * Get the status of the heater
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function heaterStatus()
    {
        return $this->hasMany(\App\Entities\HeaterStatus::class);
    }
}
