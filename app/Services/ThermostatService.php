<?php

namespace App\Services;

use App\Services\HeaterService;
use App\Exceptions\HeaterNotFoundException;
use App\Exceptions\TargetTemperatureNotFoundException;
use App\Exceptions\ObservationNotFoundException;
use App\Entities\Room;

/**
 * Thermostat Service
 */
class ThermostatService
{
    /**
     * Constructor
     *
     * @param HeaterService $heaterService
     */
    public function __construct(HeaterService $heaterService)
    {
        $this->heaterService = $heaterService;
    }

    /**
     * Handle a new Observation
     *
     * @return void
     */
    public function handle(Room $room)
    {
        $observation = $room->observation()->orderBy('created_at', 'desc')->first();
        $target = $room->target()->active()->first();
        $heater = $room->heater->first();

        if (is_null($observation)) {
            throw new ObservationNotFoundException();
        }

        if (is_null($target)) {
            throw new TargetTemperatureNotFoundException();
        }

        if (is_null($heater)) {
            throw new HeaterNotFoundException();
        }

        $observation->temperature >= $target->temperature ? $this->heaterService->turnOff($heater) : $this->heaterService->turnOn($heater);
    }
}
