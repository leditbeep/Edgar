<?php

namespace App\Services;

use App\Entities\Heater;

class HeaterService
{
    public function turnOn(Heater $heater)
    {
        // $cmd = 'mosquitto_pub -h localhost -t "heater/'.$heater->serial.' -m 1"';
        $cmd = 'mosquitto_pub -h 192.168.1.56 -t "heater/'.$heater->serial.'" -m 1';

        exec($cmd);
        $heater->heaterStatus()->create([
            'status' => true,
        ]);
    }

    public function turnOff(Heater $heater)
    {
        // $cmd = 'mosquitto_pub -h localhost -t "heater/'.$heater->serial.' -m 0"';
        $cmd = 'mosquitto_pub -h 192.168.1.56 -t "heater/'.$heater->serial.'" -m 0';

        exec($cmd);
        $heater->heaterStatus()->create([
            'status' => false,
        ]);
    }
}
