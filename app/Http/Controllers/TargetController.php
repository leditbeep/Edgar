<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Room;
use App\Entities\Target;
use App\Events\TargetCreated;
use App\Http\Requests\StoreTarget;

class TargetController extends Controller
{
    /**
     * Retrieve all target for a Room
     *
     * @param  Request $request
     * @param  Room    $room
     * @return string           list of target as Json
     */
    public function index(Request $request, Room $room)
    {
        return $room->target()->active()->get()->toJson();
    }

    /**
     * Retrieve applicable target for a Room
     *
     * @param  Request $request
     * @param  Room    $room
     * @return string           list of target as Json
     */
    public function applicable(Request $request, Room $room)
    {
        return $room->target()->active()->first()->toJson();
    }

    /**
     * Create new target
     *
     * @param  StoreTarget $request
     * @param  Room        $room
     * @return void
     */
    public function store(StoreTarget $request, Room $room)
    {
        $target = $room->target()->create($request->all());

        event(new TargetCreated($target));
    }
}
