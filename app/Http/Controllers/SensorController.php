<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Room;
use App\Http\Requests\StoreSensor;

class SensorController extends Controller
{
    /**
     * Retrieve list of sensors for a room
     *
     * @param  Request $request
     * @param  Room    $room
     * @return Json
     */
    public function index(Request $request, Room $room)
    {
        return $room->sensor()->get()->toJson();
    }

    /**
     * Store a sensor for a room
     *
     * @param  StoreSensor $request
     * @param  Room        $room
     * @return void
     */
    public function store(StoreSensor $request, Room $room)
    {
        $room->sensor()->create($request->all());
    }
}
