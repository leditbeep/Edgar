<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Room;
use App\Http\Requests\StoreHeater;

class HeaterController extends Controller
{
    public function index(Request $request, Room $room)
    {
        return $room->heater()->get()->toJson();
    }

    public function store(StoreHeater $request, Room $room)
    {
        $room->heater()->create($request->all());
    }

    public function status(Request $request, Room $room)
    {
        $response = collect(['status' => false]);

        $heaters = $room->heater()->get();

        foreach ($heaters as $heater) {
            if ($heater->heaterStatus()->orderBy('created_at', 'desc')->first()->status == '1') {
                $response['status'] = true;
                return $response->toJson();
            }
        }

        return $response->toJson();
    }
}
