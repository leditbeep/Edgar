<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Home;

class HomeController extends Controller
{
    public function index()
    {
        $homes = Home::all();

        return view('home.index')->with([
            'homes' => $homes,
        ]);
    }
}
