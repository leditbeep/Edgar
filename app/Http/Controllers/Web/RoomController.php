<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Home;
use App\Entities\Room;

class RoomController extends Controller
{
    public function index(Request $request, Home $home)
    {
        $room = $home->room()->get();

        return view('room.index')->with([
            'rooms' => $room,
        ]);
    }

    public function show(Request $request, Room $room)
    {
        return view('room.show')->with([
            'room' => $room,
        ]);
    }
}
