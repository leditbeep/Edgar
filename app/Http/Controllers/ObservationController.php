<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Observation;
use App\Entities\Room;
use App\Entities\Sensor;
use App\Http\Requests\StoreObservation;
use App\Events\ObservationCreated;

class ObservationController extends Controller
{
    /**
     * List all observations
     *
     * @return mixed Observations
     */
    public function index(Request $request, Room $room)
    {
        return $room->observation()->orderBy('created_at', 'desc')->get()->toJson();
    }

    /**
     * List all observations
     *
     * @return mixed Observations
     */
    public function last(Request $request, Room $room)
    {
        return $room->observation()->orderBy('created_at', 'desc')->first()->toJson();
    }

    /**
     * Store a new Observation
     *
     * @param  Request $request
     * @return void
     */
    public function store(StoreObservation $request, Sensor $sensor)
    {
        $observation = $sensor->observation()->create($request->all());

        event(new ObservationCreated($observation));
    }
}
