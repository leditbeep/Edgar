<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Room;
use App\Entities\Home;
use App\Http\Requests\StoreRoom;

class RoomController extends Controller
{
    public function index(Request $request, Home $home)
    {
        return $home->room()->get()->toJson();
    }

    public function store(StoreRoom $request, Home $home)
    {
        $home->room()->create($request->all());
    }
}
