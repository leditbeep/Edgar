<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Home;
use App\Http\Requests\StoreHome;

class HomeController extends Controller
{
    public function index()
    {
        return Home::all()->toJson();
    }

    public function store(StoreHome $request)
    {
        Home::create($request->all());
    }
}
