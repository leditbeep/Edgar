<?php

namespace App\Listeners;

use App\Events\ObservationCreated;
use App\Services\ThermostatService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ObservationCreationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ObservationCreated  $event
     * @return void
     */
    public function handle(ObservationCreated $event)
    {
        $thermostatService = resolve(ThermostatService::class);
        $thermostatService->handle($event->observation->sensor->room);
    }
}
