<?php

namespace App\Listeners;

use App\Events\TargetCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\ThermostatService;

class TargetCreationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TargetCreated  $event
     * @return void
     */
    public function handle(TargetCreated $event)
    {
        $thermostatService = resolve(ThermostatService::class);
        $thermostatService->handle($event->target->room);
    }
}
