# Edgar

Edgar is a web thermostat controller based on Laravel.
It exposes a REST API to the different elements: sensors, heaters, etc.
The web UI can then be used by the user to check status and manage temperatures.

## Getting started
Inside the project folder, run `docker-compose up -d` then visit `localhost:8080` on your browser

## Contributing

Contributing to Edgar is as simple as laravel can be.  Check the [Laravel Documentation](https://laravel.com/docs/).
