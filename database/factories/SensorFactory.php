<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Sensor::class, function (Faker $faker) {
    return [
        'serial' => $faker->slug,
        'name' => $faker->userName,
        'description' => $faker->sentence,
        'room_id' => function () {
            return factory(App\Entities\Room::class)->create()->id;
        }
    ];
});
