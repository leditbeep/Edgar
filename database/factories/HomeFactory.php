<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Home::class, function (Faker $faker) {
    return [
        'name' => $faker->userName,
        'description' => $faker->address,
    ];
});
