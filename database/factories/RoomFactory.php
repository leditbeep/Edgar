<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Room::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->sentence,
        'home_id' => function () {
            return factory(App\Entities\Home::class)->create()->id;
        }
    ];
});
