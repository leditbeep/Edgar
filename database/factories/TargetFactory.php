<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Entities\Target::class, function (Faker $faker) {
    return [
        'room_id' => function () {
            return factory(App\Entities\Room::class)->create()->id;
            ;
        },
        'temperature' => '20',
        'start' => Carbon::yesterday(),
        'end' => Carbon::tomorrow(),
        'type' => $faker->randomDigitNotNull,
    ];
});
