<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Observation::class, function (Faker $faker) {
    return [
        'temperature' => mt_rand(0, 100),
        'sensor_id' => function () {
            return factory(App\Entities\Sensor::class)->create()->id;
        }
    ];
});
