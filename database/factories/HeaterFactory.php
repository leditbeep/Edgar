<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Heater::class, function (Faker $faker) {
    return [
        'room_id' => function () {
            return factory(\App\Entities\Room::class)->create()->id;
        },
        'serial' => $faker->slug,
        'name' => $faker->name,
        'description' => $faker->sentence,
    ];
});
