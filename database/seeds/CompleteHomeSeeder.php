<?php

use Illuminate\Database\Seeder;

class CompleteHomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $home = factory(\App\Entities\Home::class)->create();

        $room = factory(\App\Entities\Room::class)->create([
            'home_id' => $home->id,
        ]);

        factory(\App\Entities\Heater::class)->create([
            'room_id' => $room->id,
        ]);

        $sensor = factory(\App\Entities\Sensor::class)->create([
            'room_id' => $room->id,
        ]);

        factory(\App\Entities\Target::class)->create([
            'room_id' => $room->id,
        ]);

        factory(\App\Entities\Observation::class)->create([
            'sensor_id' => $sensor->id,
        ]);
    }
}
