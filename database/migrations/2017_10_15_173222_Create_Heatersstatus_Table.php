<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeatersstatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heater_status', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('heater_id')->unsigned()->nullable();
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('heater_id')->references('id')->on('heaters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('heater_status');
    }
}
